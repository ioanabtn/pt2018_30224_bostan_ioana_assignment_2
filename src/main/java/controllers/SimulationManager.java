package controllers;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.Box;
import javax.swing.JList;
import javax.swing.JScrollPane;

import models.Customer;
import models.Scheduler;
import models.SelectionPolicy;
import models.Server;
import views.SimFrame;

public class SimulationManager implements Runnable {
	// data read from UI
	public int minArrivalTime;
	public int maxArrivalTime;
	public int minServingTime;
	public int maxServingTime;
	public int noQueues;
	public int itemsPerQueue;
	public int simulationTime;
	public int avgWTime;
	public int avgSTime;
	public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
	public int noCustomers;
	public int peakHour;
	public int maxC = 0;
	public int emptyQTime;
	//public String[] sCustomers = new String[noCustomers];
	// entity responsable with queue management and client distribution
	private Scheduler scheduler;
	// frame for displaying simulation
	private SimFrame frame = new SimFrame();
	// pool of customers
	private List<Customer> generatedCustomers = new ArrayList<Customer>();
	//private JList<String> genCustomersList;
	private List<Server> servers = new ArrayList<Server>();

	Thread t;

	public SimulationManager() {

		frame.setVisible(true);

		this.frame.addStartButtonActionListener(e -> {

			minArrivalTime = this.frame.getMinATimeTextField();
			maxArrivalTime = this.frame.getMaxATimeTextField();
			minServingTime = this.frame.getMinSTimeTextField();
			maxServingTime = this.frame.getMaxSTimeTextField();
			noQueues = this.frame.getNoQueuesTextField();
			itemsPerQueue = this.frame.getIQTextField();
			simulationTime = this.frame.getSimIntervalTextField();
			noCustomers = this.frame.getTextField();
			
			scheduler = new Scheduler(noQueues, itemsPerQueue, frame);
			
			
			servers = scheduler.getServers();
			scheduler.changeStrategy(selectionPolicy);

			generateNRandomCustomers();

			
			
			for( int i = 0; i<this.generatedCustomers.size(); i++) {
				this.frame.addClient("Clienti", String.format("Client %d (%d, %d)", generatedCustomers.get(i).getId(), generatedCustomers.get(i).getArrivalTime(), generatedCustomers.get(i).getProcessingPeriod()));
			}
			

			this.t = new Thread(this);
			this.t.start();

			

		});
	};

	private void generateNRandomCustomers() {

		for (int i = 0; i < noCustomers; i++) {

			int randomProcessingTime = (int) ((Math.random() * (maxServingTime - minServingTime + 1)) + minServingTime);
			int randomArrivalTime = (int) ((Math.random() * (maxArrivalTime - minArrivalTime + 1)) + minArrivalTime);
			Customer c = new Customer(randomArrivalTime, randomProcessingTime, i);
			generatedCustomers.add(c);
			

		}
		Collections.sort(generatedCustomers);
	}

	public void run() {

		int currentTime = 0;
		List<Customer> listToRemove = new ArrayList(); 
		while (currentTime < simulationTime) {
			this.frame.getMomLabeld().setText(String.format("%d", currentTime));
			for(Customer customer:generatedCustomers) {
				if (customer.getArrivalTime() == currentTime) {
					
					this.frame.appendLog("Customer "+ customer.getId()+ " arrived at time " + customer.getArrivalTime() + " with a processing period of "+ customer.getProcessingPeriod()+ "\n");
					
					scheduler.dispatchCustomer(customer);
					
					avgSTime = avgSTime + customer.getProcessingPeriod();
					
					listToRemove.add(customer);
					
					frame.removeFromQueue("Clienti"); 
					
				}

			}
	
			generatedCustomers.removeAll(listToRemove);
			currentTime++;
			frame.revalidate();
			frame.repaint();
			
			
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			for (Server s: servers) {
				if (s.getCustomers().size() > maxC ) {
					maxC = s.getCustomers().size();
					peakHour = currentTime;
				}
				if (s.getCustomers().size() == 0 ) {
					emptyQTime = emptyQTime + 1;
					
				}
				
			}
			
		}
		for (Server s: servers) {
			this.avgWTime = this.avgWTime + s.getWaitingTime();
		}
		
		
		avgWTime = avgWTime / noCustomers;
		
		String avgWTimeS = avgWTime + " ";
		
		this.frame.getTextField_1().setText(avgWTimeS);
		this.frame.getTextField_1().setEditable(false);		
		

		avgSTime = avgSTime / noCustomers;
		
		String avgSTimeS = avgSTime + " ";
		
		this.frame.getTextField_2().setText(avgSTimeS);
		this.frame.getTextField_2().setEditable(false);	
		
		
		
		
		String peakHourS = peakHour + " ";
		
		this.frame.getTextField_4().setText(peakHourS);
		this.frame.getTextField_4().setEditable(false);	
		
		emptyQTime = emptyQTime/3;
		String emptyQTimeS = emptyQTime + " ";
		
		this.frame.getTextField_3().setText(emptyQTimeS);
		this.frame.getTextField_3().setEditable(false);	
	}

	public static void main(String[] args) {
		SimulationManager sim = new SimulationManager();
		
	}

}
