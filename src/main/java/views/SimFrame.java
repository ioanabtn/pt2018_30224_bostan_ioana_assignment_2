package views;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import models.Customer;

public class SimFrame extends JFrame{

	
	private JTextField minATimeTextField;
	private JTextField maxATimeTextField;
	private JTextField minSTimeTextField;
	private JTextField maxSTimeTextField;
	private JTextField noQueuesTextField;
	private JTextField simIntervalTextField;
	private JButton startButton;
	private JLabel itemsQueueLabel;
	private JTextField iQTextField;
	private JPanel outputPanel;
	private JLabel timeLabel;
	private JLabel momLabel;
	private JPanel panelClienti;
	private JScrollPane scrollPane;
	private JPanel panel1;
	private JScrollPane scrollPane1;
	private JPanel panel2;
	private JScrollPane scrollPane2;
	private JPanel panel3;
	private JScrollPane scrollPane3;
	private JTextField textField;
	private JTextArea textArea;
	private JLabel lblAvgWTime;
	private JLabel lblAvgSTime;
	private JLabel lblEmptyQTime;
	private JLabel lblPeakHour;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	
	
	public SimFrame() {
	
		this.setTitle("Waiting queues simulation!");
		this.setBounds(100, 100, 579, 528);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setBounds(0, 0, 138, 354);
		this.getContentPane().add(inputPanel);
		inputPanel.setLayout(null);
		
		JLabel arrivingTimeLabel = new JLabel("Arriving Time Interval");
		arrivingTimeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		arrivingTimeLabel.setBounds(0, 5, 138, 19);
		inputPanel.add(arrivingTimeLabel);
		
		minATimeTextField = new JTextField();
		minATimeTextField.setBounds(10, 24, 47, 20);
		inputPanel.add(minATimeTextField);
		minATimeTextField.setColumns(10);
		
		maxATimeTextField = new JTextField();
		maxATimeTextField.setBounds(77, 24, 47, 20);
		inputPanel.add(maxATimeTextField);
		maxATimeTextField.setColumns(10);
		
		JLabel serviceTimeLabel = new JLabel("Service Time Interval");
		serviceTimeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		serviceTimeLabel.setBounds(0, 55, 138, 19);
		inputPanel.add(serviceTimeLabel);
		
		minSTimeTextField = new JTextField();
		minSTimeTextField.setColumns(10);
		minSTimeTextField.setBounds(10, 85, 47, 20);
		inputPanel.add(minSTimeTextField);
		
		maxSTimeTextField = new JTextField();
		maxSTimeTextField.setColumns(10);
		maxSTimeTextField.setBounds(77, 85, 47, 20);
		inputPanel.add(maxSTimeTextField);
		
		JLabel noQueuesLabel = new JLabel("No. Queues");
		noQueuesLabel.setHorizontalAlignment(SwingConstants.CENTER);
		noQueuesLabel.setBounds(10, 116, 114, 19);
		inputPanel.add(noQueuesLabel);
		
		noQueuesTextField = new JTextField();
		noQueuesTextField.setColumns(10);
		noQueuesTextField.setBounds(43, 139, 47, 20);
		inputPanel.add(noQueuesTextField);
		
		JLabel simIntervalLabel = new JLabel("Simulation Time");
		simIntervalLabel.setHorizontalAlignment(SwingConstants.CENTER);
		simIntervalLabel.setBounds(10, 213, 114, 19);
		inputPanel.add(simIntervalLabel);
		
		simIntervalTextField = new JTextField();
		simIntervalTextField.setColumns(10);
		simIntervalTextField.setBounds(43, 238, 47, 20);
		inputPanel.add(simIntervalTextField);
		
		JLabel sLabel = new JLabel("s");
		sLabel.setHorizontalAlignment(SwingConstants.CENTER);
		sLabel.setBounds(92, 241, 17, 14);
		inputPanel.add(sLabel);
		
		startButton = new JButton("START");
		startButton.setBounds(10, 312, 114, 19);
		inputPanel.add(startButton);
		
		itemsQueueLabel = new JLabel("Items/Queue");
		itemsQueueLabel.setHorizontalAlignment(SwingConstants.CENTER);
		itemsQueueLabel.setBounds(10, 164, 114, 19);
		inputPanel.add(itemsQueueLabel);
		
		iQTextField = new JTextField();
		iQTextField.setColumns(10);
		iQTextField.setBounds(43, 192, 47, 20);
		inputPanel.add(iQTextField);
		
		timeLabel = new JLabel("Time:");
		timeLabel.setBounds(10, 340, 46, 14);
		inputPanel.add(timeLabel);
		
		momLabel = new JLabel("");
		momLabel.setBounds(63, 340, 46, 14);
		inputPanel.add(momLabel);
		
		JLabel lblNoCustomers = new JLabel("No. Customers");
		lblNoCustomers.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoCustomers.setBounds(10, 269, 114, 19);
		inputPanel.add(lblNoCustomers);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(43, 288, 47, 20);
		inputPanel.add(textField);
		
		outputPanel = new JPanel();
		outputPanel.setBounds(136, 0, 468, 354);
		getContentPane().add(outputPanel);
		outputPanel.setLayout(null);
		
	
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 103, 325);
		outputPanel.add(scrollPane);
		
		panelClienti = new JPanel();
		scrollPane.setViewportView(panelClienti);
		panelClienti.setLayout(new BoxLayout(panelClienti,BoxLayout.Y_AXIS));
		
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(113, 11, 103, 325);
		outputPanel.add(scrollPane1);
		
		panel1 = new JPanel();
		scrollPane1.setViewportView(panel1);
		panel1.setLayout(new BoxLayout(panel1,BoxLayout.Y_AXIS));
		
		scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(213, 11, 103, 325);
		outputPanel.add(scrollPane2);
		
		panel2 = new JPanel();
		scrollPane2.setViewportView(panel2);
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.Y_AXIS));
		
		scrollPane3 = new JScrollPane();
		scrollPane3.setBounds(313, 11, 103, 325);
		outputPanel.add(scrollPane3);
		
		panel3 = new JPanel();
		scrollPane3.setViewportView(panel3);
		panel3.setLayout(new BoxLayout(panel3,BoxLayout.Y_AXIS));
		
		JLabel lblQueue = new JLabel("Queue1");
		lblQueue.setBounds(140, 340, 46, 14);
		outputPanel.add(lblQueue);
		
		JLabel lblQueue_1 = new JLabel("Queue2");
		lblQueue_1.setBounds(241, 340, 46, 14);
		outputPanel.add(lblQueue_1);
		
		JLabel lblQueue_2 = new JLabel("Queue3");
		lblQueue_2.setBounds(341, 340, 46, 14);
		outputPanel.add(lblQueue_2);
		
		JLabel lblGencustomers = new JLabel("genCustomers");
		lblGencustomers.setHorizontalAlignment(SwingConstants.CENTER);
		lblGencustomers.setBounds(10, 340, 103, 14);
		outputPanel.add(lblGencustomers);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 354, 357, 135);
		getContentPane().add(scrollPane_1);
		
		textArea = new JTextArea();
		scrollPane_1.setViewportView(textArea);
		
		lblAvgWTime = new JLabel("Avg W Time");
		lblAvgWTime.setBounds(378, 365, 67, 14);
		getContentPane().add(lblAvgWTime);
		
		lblAvgSTime = new JLabel("Avg S Time");
		lblAvgSTime.setBounds(378, 400, 67, 14);
		getContentPane().add(lblAvgSTime);
		
		lblEmptyQTime = new JLabel("Empty Q Time");
		lblEmptyQTime.setBounds(378, 432, 93, 14);
		getContentPane().add(lblEmptyQTime);
		
		lblPeakHour = new JLabel("Peak Hour");
		lblPeakHour.setBounds(378, 464, 67, 14);
		getContentPane().add(lblPeakHour);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(469, 362, 47, 20);
		getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(469, 397, 47, 20);
		getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(469, 429, 47, 20);
		getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(469, 461, 47, 20);
		getContentPane().add(textField_4);
	
		
	}
	
	public void addClient(String coada, String client) {
		JLabel lbl = new JLabel(client);
		lbl.setAlignmentX(CENTER_ALIGNMENT); 
		
		switch (coada) {
		case "Clienti":
			this.panelClienti.add(Box.createRigidArea(new Dimension(15, 0)));  
			this.panelClienti.add(lbl);
			break;
		case "Server 0": 
			this.panel1.add(Box.createRigidArea(new Dimension(15, 0)));
			this.panel1.add(lbl);
			break;
		case "Server 1":
			this.panel2.add(Box.createRigidArea(new Dimension(15, 0)));
			this.panel2.add(lbl);
			break;
			
		case "Server 2":
			this.panel3.add(Box.createRigidArea(new Dimension(15, 0)));
			this.panel3.add(lbl);
			break;
		case "Server 3":
			
			break;
		case "Server 4":
			
			break;
		}

		this.revalidate();
		this.repaint();
	}
	
	
	
	public void removeFromQueue(String coada) {

		switch (coada) {
		case "Clienti": 
			this.panelClienti.remove(0);
			this.panelClienti.remove(0);
			break;
		case "Server 1":
			this.panel1.remove(0);
			this.panel1.remove(0);
			break;
		case "Server 2": 
			this.panel2.remove(0);
			this.panel2.remove(0);
			break;
		case "Server 3":
			this.panel3.remove(0);
			this.panel3.remove(0);
			break;
		case "Server 4":
			
			break;
		case "Server 5":
			
			break;
		}

		this.revalidate();
		this.repaint();
	}
	
	public JPanel getPanelClienti() {
		return panelClienti;
	}

	public void setPanelClienti(JPanel panelClienti) {
		this.panelClienti = panelClienti;
	}

	public int getMinATimeTextField() {
		return Integer.parseInt(minATimeTextField.getText());
	}
	public void setMinATimeTextField(JTextField minATimeTextField) {
		this.minATimeTextField = minATimeTextField;
	}
	
	public int getMaxATimeTextField() {
		return Integer.parseInt(maxATimeTextField.getText());
	}
	public void setMaxATimeTextField(JTextField maxATimeTextField) {
		this.maxATimeTextField = maxATimeTextField;
	}
	
	public int getMinSTimeTextField() {
		return Integer.parseInt(minSTimeTextField.getText());
	}
	public void setMinSTimeTextField(JTextField minSTimeTextField) {
		this.minSTimeTextField = minSTimeTextField;
	}
	
	
	public int getMaxSTimeTextField() {
		return Integer.parseInt(maxSTimeTextField.getText());
	}
	public void setMaxSTimeTextField(JTextField maxSTimeTextField) {
		this.maxSTimeTextField = maxSTimeTextField;
	}
	
	public int getNoQueuesTextField() {
		return Integer.parseInt(noQueuesTextField.getText());
	}
	public void setNoQueuesTextField(JTextField noQueuesTextField) {
		this.noQueuesTextField = noQueuesTextField;
	}
	
	public JLabel getMomLabeld() {
		return momLabel;
	}
	public void setMomLabel(JLabel momLabel) {
		this.momLabel = momLabel;
	}
	
	public int getSimIntervalTextField() {
		return Integer.parseInt(simIntervalTextField.getText());
	}
	public void setSimIntervalTextField(JTextField simIntervalTextField) {
		this.simIntervalTextField = simIntervalTextField;
	}
	
	public void addStartButtonActionListener(ActionListener al) {
		this.startButton.addActionListener(al);
	}
	
	
	
	public int getIQTextField() {
		return Integer.parseInt(iQTextField.getText());
	}
	public void setIQTextField(JTextField iQTextField) {
		this.iQTextField =iQTextField;
	}
	
	public JPanel getOutputPanel() {
		return outputPanel;
	}
	public void setOutputPanel(JPanel outputPanel) {
		this.outputPanel = outputPanel;
	}
	
	public int getTextField() {
		return Integer.parseInt(textField.getText());
	}
	
	public JTextField getTextField_1() {
		return textField_1;
	}
	
	public JTextField getTextField_2() {
		return textField_2;
	}
	
	public JTextField getTextField_3() {
		return textField_3;
	}
	public JTextField getTextField_4() {
		return textField_4;
	}
	
	public synchronized void  appendLog(String s) {
		this.textArea.setText(this.textArea.getText() + s);
	}
}
