package models;

import java.util.List;

import views.SimFrame;

public class ConcreteStrategyQueue implements Strategy {

	public void addCustomer(List<Server> servers, Customer c, SimFrame frame ) {
		int minim = Integer.MAX_VALUE;
		int aux = 0;
		
		for(int i = 0; i < servers.size(); i++) {
			if(servers.get(i).getCustomers().size()<minim) {
				minim = servers.get(i).getCustomers().size();
				aux = i;
			}
		}
		
		for(int i = 0; i<servers.size(); i++) {
			if(aux == i) {
				servers.get(i).addCustomer(c);
				String serverName = "Server" + aux;
				frame.addClient(serverName, String.format("Client %d (%d, %d)", c.getId(),c.getArrivalTime(), c.getProcessingPeriod()));
				frame.appendLog("Customer "+ c.getId() + "entry in queue "+ servers.get(i).getServerId()+ "/n");
			}
		}
	}
}
