package models;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import views.SimFrame;

public class ConcreteStrategyTime implements Strategy{
	
	public void addCustomer(List<Server> servers, Customer c, SimFrame frame) {
		AtomicInteger minim = new AtomicInteger(1024);
		int aux = 0;
		for(int i=0; i<servers.size(); i++) {
			if(servers.get(i).getWaitingPeriod().get()<minim.get()) {
				minim = servers.get(i).getWaitingPeriod();
				aux = i;
			}
		}
		
		for(int i=0; i<servers.size(); i++) {
			if (aux == i) {
				servers.get(i).addCustomer(c);
				
				String serverName = "Server " + aux;
				frame.addClient(serverName, String.format("Client %d (%d, %d)", c.getId(),c.getArrivalTime(), c.getProcessingPeriod()));
				frame.appendLog("Customer "+ c.getId() + " enters in queue "+ servers.get(i).getServerId()+ "\n");
			}
		}
	}
}
