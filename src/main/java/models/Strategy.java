package models;

import java.util.List;

import views.SimFrame;

public interface Strategy {
	
	public void addCustomer(List<Server> servers, Customer c, SimFrame sim);
}
