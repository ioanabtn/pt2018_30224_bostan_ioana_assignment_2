package models;

import java.util.ArrayList;
import java.util.List;

import views.SimFrame;

public class Scheduler {
	
	private List<Server> servers = new ArrayList();
	private Strategy strategy;
	private SimFrame frame;
	
	
	public Scheduler(int maxNoServers, int maxCusomersPerServer, SimFrame frame) {
		this.frame = frame;
		for(int i = 0; i<maxNoServers; i++) {
			Server s = new Server(frame, i);
			servers.add(s);
			(new Thread(s)).start();
		}
	}
	
	public void changeStrategy(SelectionPolicy policy) {
		if(policy == SelectionPolicy.SHORTEST_QUEUE) {
			strategy = new ConcreteStrategyQueue();
		}
		if(policy == SelectionPolicy.SHORTEST_TIME) {
			strategy = new ConcreteStrategyTime();
		}
	}
	
	public void dispatchCustomer(Customer c) {
		strategy.addCustomer(servers, c, frame);
	}
	
	public List<Server> getServers(){
		return servers;
	}
	
}
