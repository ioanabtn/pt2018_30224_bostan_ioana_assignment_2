package models;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import views.SimFrame;

public class Server implements Runnable{
	
	private BlockingQueue<Customer> customers;
	private AtomicInteger waitingPeriod;
	private int serverId;
	private SimFrame frame;
	private int waitingTime;
	
	
	public Server(SimFrame frame, int serverId) {
		this.setServerId(serverId);
		this.frame = frame;
		customers = new ArrayBlockingQueue<Customer>(30);
		waitingPeriod = new AtomicInteger(0);
	}
	
	public void addCustomer(Customer newCustomer) {
		this.customers.add(newCustomer);
		waitingPeriod.incrementAndGet();
		
	}
	
	public void run() {
		
		
		try {
			
			while(true) {
				Customer nextCustomer = customers.take();
				Thread.sleep(nextCustomer.getProcessingPeriod()*1000);
				//Thread.sleep(10);
				waitingTime = waitingTime + this.getWaitingPeriod().get();
				String serverName;
				serverName = "Server " + this.getServerId();
				this.frame.removeFromQueue(serverName);
				
				waitingPeriod.decrementAndGet();
				int serviceTime = nextCustomer.getProcessingPeriod() + this.getWaitingPeriod().get();
				frame.appendLog("Customer "+ nextCustomer.getId() + " gets served in "+ serviceTime  + "\n");
				
				
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public BlockingQueue<Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(BlockingQueue<Customer> customers) {
		this.customers = customers;
	}
	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}
	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public int getServerId() {
		return serverId+1;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}
	
	public int getWaitingTime() {
		return this.waitingTime;
	}
	
}
