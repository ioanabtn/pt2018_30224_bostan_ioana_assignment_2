package models;

public class Customer implements Comparable<Customer>{
	
	private int arrivalTime;
	private int processingPeriod;
	private int finishTime;
	private int id;
	

	public Customer (int arrivalTime, int processingPeriod, int id) {
		this.arrivalTime = arrivalTime;
		this.processingPeriod=processingPeriod;
		this.setId(id);
		
	}
	
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getProcessingPeriod() {
		return processingPeriod;
	}
	public void setProcessingPeriod(int processingPeriod) {
		this.processingPeriod = processingPeriod;
	}
	public int getFinishTime() {
		this.finishTime = this.finishTime + this.getArrivalTime() + this.getProcessingPeriod();
		return this.finishTime;
	}
	
	public String toString() {
		String sFinishTime = "";
		 if(this.getArrivalTime()!= 0) {
			 sFinishTime = getFinishTime() + " ";
		 }
		 
		 return sFinishTime;
	}
	 public int compareTo(Customer compares) {
	        int arrivalTime=((Customer)compares).getArrivalTime();
	        return this.arrivalTime-arrivalTime;

	    }


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	
}
